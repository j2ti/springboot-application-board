package com.example.springbootapplicationboard.demo.webservice.service;

import com.example.springbootapplicationboard.demo.webservice.domain.posts.PostsRepository;
import com.example.springbootapplicationboard.demo.webservice.dto.posts.PostsSaveRequestDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@AllArgsConstructor
@Service
public class PostsService {
    private PostsRepository postsRepository;

    @Transactional
    public Long save(PostsSaveRequestDto dto){
        return postsRepository.save(dto.toEntity()).getId();
    }
}