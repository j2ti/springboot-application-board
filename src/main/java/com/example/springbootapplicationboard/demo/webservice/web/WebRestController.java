package com.example.springbootapplicationboard.demo.webservice.web;

import com.example.springbootapplicationboard.demo.webservice.domain.posts.PostsRepository;
import com.example.springbootapplicationboard.demo.webservice.dto.posts.PostsSaveRequestDto;
import com.example.springbootapplicationboard.demo.webservice.service.PostsService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class WebRestController {

//    private PostsRepository postsRepository;
    private PostsService postsService;

    @GetMapping("/hello")
    public String hello() {
        return "HelloWorld";
    }

    @PostMapping("/posts")
    public void savePosts(@RequestBody PostsSaveRequestDto dto){
        postsService.save(dto);
    }

}
